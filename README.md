# Play With Cats
## What is Play With Cats?
Play with cats is a project with the goal of increasing cat shelter adoptions and donations.
Inspired by Twitch Plays Pokemon, users control a robot mouse toy over the internet to play with cats. The mouse can be controlled by individuals via the website, or by a huge number of users with twitch streaming integration.
The users can use the two camera streams (one from the robots perspective and one from overhead) to manipulate the toy and watch the cats reactions.

## This sounds cool! Can I see a working demo?
Sure! There was a test stream which can be viewed here: https://www.twitch.tv/videos/804899977?t=00h07m20s

And an informational video which can be viewed here: https://youtu.be/HWtyuBZOBvQ

## So what does this code do?
This code is for the backend component of the project. The backend runs on Flask, utilising Gunicorn and web sockets. The backend takes movment commands from the frontend and sends them on to the rat, handles frames from both the rat and camera clients, sending them on to the frontend, and manages the user queue.

## Is this site currently live?
Not at the moment! But I may set it up again some time in the future :)
