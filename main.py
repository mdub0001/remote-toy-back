from flask import Flask, request
from flask_socketio import SocketIO, emit
from flask_cors import CORS
import json
import time

app = Flask(__name__)
CORS(app)
socketio = SocketIO(app, cors_allowed_origins="*")

queued_clients = []
MAX_TIMER = 60
active_timer = MAX_TIMER
connected_users = 0
camera_frame_timer = 0
rat_frame_timer = 0

# If user is first to connect to application, trigger streams
@socketio.on('connect', namespace='/user')
def handle_connect():
    global connected_users
    connected_users+=1
    if connected_users == 1:
        socketio.emit('video', {'stream': True}, namespace='/camera')
        socketio.emit('video', {'stream': True}, namespace='/rat')


# On camera connection, trigger stream if clients are already present
@socketio.on('connect', namespace='/camera')
def handle_connect_camera():
    if connected_users > 0:
        emit('video', {'stream': True}, namespace='/camera')


# On rat connection, trigger stream if clients are already present
@socketio.on('connect', namespace='/rat')
def handle_connect_camera():
    if connected_users > 0:
        emit('video', {'stream': True}, namespace='/rat')


# On user disconnect, manage queue
# If no other clients are connected, turn off videos streams
@socketio.on('disconnect', namespace='/user')
def handle_disconnect_user():
    global active_timer, connected_users
    connected_users-=1

    print("Client disconnect")

    # If the disconnected client was in queue, send updated queue positions to frontend
    if request.sid in queued_clients:
        queued_clients.remove(request.sid)
        for i in range(len(queued_clients)):
            socketio.emit('queue_position', {'queue_position': i}, namespace='/user', room=queued_clients[i])

    print("queued clients: " + str(len(queued_clients)))

    # If no clients are connected, turn off streams
    if connected_users == 0:
        socketio.emit('video', {'stream': False}, namespace='/camera')
        socketio.emit('video', {'stream': False}, namespace='/rat')
        return

    # If disconnected was controlling rat, reset timer and stop rat
    if is_active_client():
        active_timer = MAX_TIMER
        socketio.emit('stop', namespace='/rat')


# Infrom frontend of Rat disconnect to display offline message
@socketio.on('disconnect', namespace='/rat')
def handle_disconnect_rat():
    socketio.emit('rat_disconnected', namespace='/user', include_self=False)


# Inform frontend of camera disconnect to display offline message
@socketio.on('disconnect', namespace='/camera')
def handle_disconnect():
    socketio.emit('camera_disconnected', namespace='/user', include_self=False)


# Adds user to the queue and infrom frontend of queue position
@socketio.on('queue', namespace='/user')
def handle_queue():
    queued_clients.append(request.sid)
    emit('queue_position', {'queue_position':len(queued_clients)-1}, namespace='/user')
    print("Joined queue: " + str(len(queued_clients)-1))


############### Rat Control Events ###############
@socketio.on('forward', namespace='/user')
def handle_forward():
    if is_active_client():
        print('forward')
        socketio.emit('forward', namespace='/rat')


@socketio.on('left', namespace='/user')
def handle_left():
    if is_active_client():
        print('left')
        socketio.emit('left', namespace='/rat')


@socketio.on('right', namespace='/user')
def handle_right():
    if is_active_client():
        print('right')
        socketio.emit('right', namespace='/rat')


@socketio.on('backward', namespace='/user')
def handle_backward():
    if is_active_client():
        print('backward')
        socketio.emit('backward', namespace='/rat')


@socketio.on('stop', namespace='/user')
def handle_stop():
    if is_active_client():
        print('stop')
        socketio.emit('stop', namespace='/rat')


# Take frames from camera.
# Enforce frame limiting by calculating time difference between frames 
# and sleeping remaining time.
@socketio.on('camera_frame', namespace='/camera')
def handle_frames(frame):
    global camera_frame_timer
    fps = 1/15 # 15fps
    start_timer = time.perf_counter()
    socketio.emit('camera_frame', frame, namespace='/user', include_self=False)
    
    # If enough times hasn't elapsed for frame limit, sleep reamining time
    if start_timer - camera_frame_timer < fps:
        socketio.sleep(fps - (start_timer - camera_frame_timer))

    emit('ready', namespace='/camera')
    camera_frame_timer = time.perf_counter()


# Take frames from Rat.
# Enforce frame limiting by calculating time difference between frames 
# and sleeping remaining time.
@socketio.on('rat_frame', namespace='/rat')
def handle_frames(frame):
    global rat_frame_timer
    fps = 1/30 # 30fps
    start_timer = time.perf_counter()
    socketio.emit('rat_frame', frame, namespace='/user', include_self=False)
    if start_timer - rat_frame_timer < fps:
        socketio.sleep(fps - (start_timer - rat_frame_timer))

    emit('ready', namespace='/rat')
    rat_frame_timer = time.perf_counter()


# Play loop
# Handles play time countdown and which users is in control of rat.
def play_timer():
    global active_timer
    while True:
        socketio.sleep(2)

        # If there are no users queued, do nothing
        if len(queued_clients) == 0:
            active_timer = MAX_TIMER
            continue

        # Decrement timer
        active_timer -=2
        socketio.emit('active_timer', {'active_timer':active_timer}, namespace='/user')

        # When play time runs out, push to back of queue and reset the timer for the next player
        if active_timer <= 0:
            if len(queued_clients) > 1:
                queued_clients.append(queued_clients.pop(0))
                socketio.emit('stop', namespace='/rat')
                for i in range(len(queued_clients)):
                    socketio.emit('queue_position', {'queue_position': i}, namespace='/user', room=queued_clients[i])

            active_timer = MAX_TIMER


# Checks if the current event user is in control of the rat
def is_active_client():
    if queued_clients and request.sid == queued_clients[0]:
        return True

    return False


socketio.start_background_task(play_timer)


